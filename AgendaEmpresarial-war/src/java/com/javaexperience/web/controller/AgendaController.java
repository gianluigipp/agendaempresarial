package com.javaexperience.web.controller;

import com.javaexperience.negocio.entidades.Contacto;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gianluigi Pierini
 */
@Named(value = "agenda")
@SessionScoped
public class AgendaController implements Serializable {

    /*
     * VARIABLES PUBLICAS
     */
    private Contacto contacto = new Contacto();
    private Contacto contactoSeleccionado = new Contacto();
    private List<Contacto> listaContactos = new ArrayList<>();

    /*
     * METODOS PUBLICOS
     */
    public void prepararCrearContacto() {
        contacto = new Contacto();
    }

    public void agregarContacto() {
        listaContactos.add(contacto);
    }

    public void actualizarContacto() {

    }
    
    public void eliminarContacto(){
        listaContactos.remove(contactoSeleccionado);
    }

    /*
     * GETTERS Y SETTERS
     */
    public Contacto getContactoSeleccionado() {
        return contactoSeleccionado;
    }

    public void setContactoSeleccionado(Contacto contactoSeleccionado) {
        this.contactoSeleccionado = contactoSeleccionado;
    }

    public Contacto getContacto() {
        return contacto;
    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    public List<Contacto> getListaContactos() {
        return listaContactos;
    }

    public void setListaContactos(List<Contacto> listaContactos) {
        this.listaContactos = listaContactos;
    }
}
