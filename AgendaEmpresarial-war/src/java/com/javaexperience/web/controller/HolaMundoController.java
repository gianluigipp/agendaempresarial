package com.javaexperience.web.controller;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Gianluigi Pierini
 */
@Named(value = "holaMundo")
@ViewScoped
public class HolaMundoController {

    private String nombre;
    private String saludo = "Hola";

    public void saludar() {
        saludo = "Hola " + nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSaludo() {
        return saludo;
    }

    public void setSaludo(String saludo) {
        this.saludo = saludo;
    }
}
