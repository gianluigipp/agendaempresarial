package com.javaexperience.negocio.entidades;

import java.util.Objects;

/**
 *
 * @author Gianluigi Pierini
 */
public class Contacto {

    /*
     * VARIABLES PUBLICAS
     */
    private String nombre;
    private String telefonoPrincipal;
    private String telefonoAlternatico;
    private String correoElectronico;
    private String direccion;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contacto other = (Contacto) obj;
        return Objects.equals(this.nombre, other.nombre);
    }    
    
    /*
     * GETTERS Y SETTERS
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefonoPrincipal() {
        return telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    public String getTelefonoAlternatico() {
        return telefonoAlternatico;
    }

    public void setTelefonoAlternatico(String telefonoAlternatico) {
        this.telefonoAlternatico = telefonoAlternatico;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
